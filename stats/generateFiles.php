<?php
// include '../lib/fpdf/fpdf.php';
require('../lib/fpdf/fpdf.php');

class GenerateFiles{

  function GenerateFiles($idUser, $userName){
    $this->idUser = $idUser;
    $this->userName = $userName;

    $this->sqlInterrogation = new SQLInterrogation();
  }

  /* generate pdf file
  * returns file name
  **/
  function getPDF(){

    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(100, 8, $this->userName.' - STATS - PDF',0);
    $pdf->Ln(8);
    $pdf->Ln(15);
    $pdf->SetFont('Arial','',8);

    $this->sqlInterrogation = new SQLInterrogation();

    $result = 'Total amount: '.$this->sqlInterrogation->getSumTotal().'$';
    $pdf->Cell(80,8,$result,0);
    $pdf->Ln(8);

    $result = 'Last day: '.$this->sqlInterrogation->getSumLastDay().'$';
    $pdf->Cell(80,8,$result,0);
    $pdf->Ln(8);

    $result = 'Last week: '.$this->sqlInterrogation->getSumLastWeek().'$';
    $pdf->Cell(80,8,$result,0);
    $pdf->Ln(8);

    $result = 'Last month: '.$this->sqlInterrogation->getSumLastMonth().'$';
    $pdf->Cell(80,8,$result,0);
    $pdf->Ln(20);


    $result = $this->sqlInterrogation->getObjectsFavourites();
    $pdf->Cell(80,8,'Favourite Objects: '.count($result),1);
    if(count($result)==0){
      $pdf->Cell(80,8,'0',0);
      $pdf->Ln(8);
    }else{
      $pdf->Ln(8);
      for ($i = 0; $i < count($result); $i++) {
        $pdf->Ln(4);
        $pdf->Cell(80,30,$pdf->image('..'.$result[$i]['image'], $pdf->GetX(), $pdf->GetY(), 30, 30),0);
        $pdf->Ln(30);
        $pdf->Cell(80,8,'Name : '.$result[$i]['name'].'         Price : '.$result[$i]['price'].' $',0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Category : '.$result[$i]['category'].' >> Subcategory : '.$result[$i]['subcategory'],0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Expire Date : '.date('d-M-Y', (int)$result[$i]['enddate']),0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'--------------------------------------------------',0);
        $pdf->Ln(8);
      }
    }

    $result = $this->sqlInterrogation->getObjectsForSale($_SESSION['idUser']);
    $pdf->Ln(8);
    $pdf->Cell(80,8,'Objects for sale: '.count($result),1);
    if(count($result)==0){
      $pdf->Cell(80,8,'0',0);
      $pdf->Ln(8);
    }else{
      $pdf->Ln(8);
      for ($i = 0; $i < count($result); $i++) {
        $pdf->Ln(4);
        $pdf->Cell(80,30,$pdf->image('..'.$result[$i]['image'], $pdf->GetX(), $pdf->GetY(), 30, 30),0);
        $pdf->Ln(30);
        $pdf->Cell(80,8,'Name : '.$result[$i]['name'].'         Price : '.$result[$i]['price'].' $',0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Category : '.$result[$i]['category'].'   >>   Subcategory : '.$result[$i]['subcategory'],0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Expire Date : '.date('d-M-Y', (int)$result[$i]['enddate']),0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'--------------------------------------------------',0);
        $pdf->Ln(8);
      }
    }

    $result = $this->sqlInterrogation->getObjectsPurchased($_SESSION['idUser']);
    $pdf->Cell(80,8,'Objects purchased: '.count($result),1);
    $pdf->Ln(8);
    if(count($result)==0){
        $pdf->Cell(80,8,'None',0);
        $pdf->Ln(8);
    }else{
      $pdf->Ln(8);
      for ($i = 0; $i < count($result); $i++) {
        $pdf->Ln(4);
        $pdf->Cell(80,30,$pdf->image('..'.$result[$i]['image'], $pdf->GetX(), $pdf->GetY(), 30, 30),0);
        $pdf->Ln(30);
        $pdf->Cell(80,8,'Name : '.$result[$i]['name'].'         Price : '.$result[$i]['price'].' $',0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Category : '.$result[$i]['category'].'   >>   Subcategory : '.$result[$i]['subcategory'],0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Expire Date : '.date('d-M-Y', (int)$result[$i]['enddate']),0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'--------------------------------------------------',0);
        $pdf->Ln(8);
      }
    }


    $result = $this->sqlInterrogation->getObjectsOngoingAuction($_SESSION['idUser']);
    $pdf->Cell(80,8,'Ongoing Auction: '.count($result),1);
    $pdf->Ln(8);
    if(count($result)==0){
      $pdf->Cell(80,8,'None',0);
      $pdf->Ln(8);
    }else{
      $pdf->Ln(8);
      for ($i = 0; $i < count($result); $i++) {
        $pdf->Ln(4);
        $pdf->Cell(80,30,$pdf->image('..'.$result[$i]['image'], $pdf->GetX(), $pdf->GetY(), 30, 30),0);
        $pdf->Ln(30);
        $pdf->Cell(80,8,'Name : '.$result[$i]['name'].'         Price : '.$result[$i]['price'].' $',0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Category : '.$result[$i]['category'].'   >>   Subcategory : '.$result[$i]['subcategory'],0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Expire Date : '.date('d-M-Y', (int)$result[$i]['enddate']),0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'--------------------------------------------------',0);
        $pdf->Ln(8);
      }
    }


    $result = $this->sqlInterrogation->getObjectsSold($_SESSION['idUser']);
    $pdf->Cell(80,8,'Sold items: '.count($result),1);
    $pdf->Ln(8);
    if(count($result)==0){
      $pdf->Cell(80,8,'None',0);
      $pdf->Ln(8);
    }else{
      $pdf->Ln(8);
      for ($i = 0; $i < count($result); $i++) {
        $pdf->Ln(4);
        $pdf->Cell(80,30,$pdf->image('..'.$result[$i]['image'], $pdf->GetX(), $pdf->GetY(), 30, 30),0);
        $pdf->Ln(30);
        $pdf->Cell(80,8,'Name : '.$result[$i]['name'].'         Price : '.$result[$i]['price'].' $',0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Category : '.$result[$i]['category'].'   >>   Subcategory : '.$result[$i]['subcategory'],0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'Expire Date : '.date('d-M-Y', (int)$result[$i]['enddate']),0);
        $pdf->Ln(8);
        $pdf->Cell(80,8,'--------------------------------------------------',0);
        $pdf->Ln(8);
      }
    }

    $filename = $this->idUser.'_stats.pdf';
    $pdf->Output($filename,'F');
    $pdf = null;

    return $filename;
  }

  /* generate json file
  * returns file name
  **/
  function getXML(){

    	$xml=new DOMDocument('1.0');
    	$xml->formatOutput=true;

    	$statsPageXML=$xml->createElement("statsPageXML");
    	$xml->appendChild($statsPageXML);

    	$totalAmount=$xml->createElement("totalAmount");
    	$result = $this->sqlInterrogation->getSumTotal();
    	$totalAmount->setAttribute("nrObjects", $result);
    	$statsPageXML->appendChild($totalAmount);

    	$lastDay=$xml->createElement("sumLastDay");
    	$result = $this->sqlInterrogation->getSumLastDay();
    	$lastDay->setAttribute("nrObjects", $result);
    	$statsPageXML->appendChild($lastDay);

    	$lastWeek=$xml->createElement("sumLastWeek");
    	$result = $this->sqlInterrogation->getSumLastWeek();
    	$lastWeek->setAttribute("nrObjects", $result);
    	$statsPageXML->appendChild($lastWeek);

    	$lastMonth=$xml->createElement("sumLastMonth");
    	$result = $this->sqlInterrogation->getSumLastMonth();
    	$lastMonth->setAttribute("nrObjects", $result);
    	$statsPageXML->appendChild($lastMonth);

    	$favouriteObjects=$xml->createElement("favouriteObjects");
    	$result = $this->sqlInterrogation->getObjectsFavourites();
    	if(count($result)==0){
            $favouriteObjects->setAttribute("nrObjects", 0);

        }else{
          $favouriteObjects->setAttribute("nrObjects", count($result));
          for ($i = 0; $i < count($result); $i++) {
            $objectCounter=$xml->createElement("object");
            $objectCounter->setAttribute("id",$result[$i]['idObject']);
            $favouriteObjects->appendChild($objectCounter);

            $name=$xml->createElement("name");
            $name->setAttribute("value",$result[$i]['name']);
            $objectCounter->appendChild($name);

            $category=$xml->createElement("category");
            $category->setAttribute("value",$result[$i]['category']);
            $objectCounter->appendChild($category);

            $subcategory=$xml->createElement("subcategory");
            $subcategory->setAttribute("value",$result[$i]['subcategory']);
            $objectCounter->appendChild($subcategory);

            $price=$xml->createElement("price");
            $price->setAttribute("value",$result[$i]['price']);
            $objectCounter->appendChild($price);

            $expireDate=$xml->createElement("date");
            $expireDate->setAttribute("value",date('d-M-Y', (int)$result[$i]['enddate']));
            $objectCounter->appendChild($expireDate);
          }
        }
    	$statsPageXML->appendChild($favouriteObjects);

    	$objectsForSale=$xml->createElement("objectsForSale");
    	$result = $this->sqlInterrogation->getObjectsForSale($_SESSION['idUser']);
    	if(count($result)==0){
            $objectsForSale->setAttribute("nrObjects", 0);
        }else{
          $objectsForSale->setAttribute("nrObjects", count($result));
          for ($i = 0; $i < count($result); $i++) {
            $objectCounter=$xml->createElement("object");
            $objectCounter->setAttribute("id",$result[$i]['idObject']);
            $objectsForSale->appendChild($objectCounter);

            $name=$xml->createElement("name");
            $name->setAttribute("value",$result[$i]['name']);
            $objectCounter->appendChild($name);

            $category=$xml->createElement("category");
            $category->setAttribute("value",$result[$i]['category']);
            $objectCounter->appendChild($category);

            $subcategory=$xml->createElement("subcategory");
            $subcategory->setAttribute("value",$result[$i]['subcategory']);
            $objectCounter->appendChild($subcategory);

            $price=$xml->createElement("price");
            $price->setAttribute("value",$result[$i]['price']);
            $objectCounter->appendChild($price);

            $expireDate=$xml->createElement("date");
            $expireDate->setAttribute("value",date('d-M-Y', (int)$result[$i]['enddate']));
            $objectCounter->appendChild($expireDate);
          }
        }
    	$statsPageXML->appendChild($objectsForSale);

    	$purchasedObjects=$xml->createElement("purchasedObjects");
    	$result = $this->sqlInterrogation->getObjectsPurchased($_SESSION['idUser']);
    	if(count($result)==0){
          $purchasedObjects->setAttribute("nrObjects", 0);
        }else{
          $purchasedObjects->setAttribute("nrObjects", count($result));
          for ($i = 0; $i < count($result); $i++) {
            $objectCounter=$xml->createElement("object");
            $objectCounter->setAttribute("id",$result[$i]['idObject']);
            $purchasedObjects->appendChild($objectCounter);

            $name=$xml->createElement("name");
            $name->setAttribute("value",$result[$i]['name']);
            $objectCounter->appendChild($name);

            $category=$xml->createElement("category");
            $category->setAttribute("value",$result[$i]['category']);
            $objectCounter->appendChild($category);

            $subcategory=$xml->createElement("subcategory");
            $subcategory->setAttribute("value",$result[$i]['subcategory']);
            $objectCounter->appendChild($subcategory);

            $price=$xml->createElement("price");
            $price->setAttribute("value",$result[$i]['price']);
            $objectCounter->appendChild($price);

            $expireDate=$xml->createElement("date");
            $expireDate->setAttribute("value",date('d-M-Y', (int)$result[$i]['enddate']));
            $objectCounter->appendChild($expireDate);
          }
        }
    	$statsPageXML->appendChild($purchasedObjects);


    	$ongoingAuctionObjects=$xml->createElement("ongoingAuctionObjects");
    	$result = $this->sqlInterrogation->getObjectsOngoingAuction($_SESSION['idUser']);
    	if(count($result)==0){
          $ongoingAuctionObjects->setAttribute("nrObjects", 0);
        }else{
          $ongoingAuctionObjects->setAttribute("nrObjects", count($result));
          for ($i = 0; $i < count($result); $i++) {
            $objectCounter=$xml->createElement("object");
            $objectCounter->setAttribute("id",$result[$i]['idObject']);
            $ongoingAuctionObjects->appendChild($objectCounter);

            $name=$xml->createElement("name");
            $name->setAttribute("value",$result[$i]['name']);
            $objectCounter->appendChild($name);

            $category=$xml->createElement("category");
            $category->setAttribute("value",$result[$i]['category']);
            $objectCounter->appendChild($category);

            $subcategory=$xml->createElement("subcategory");
            $subcategory->setAttribute("value",$result[$i]['subcategory']);
            $objectCounter->appendChild($subcategory);

            $price=$xml->createElement("price");
            $price->setAttribute("value",$result[$i]['price']);
            $objectCounter->appendChild($price);

            $expireDate=$xml->createElement("date");
            $expireDate->setAttribute("value",date('d-M-Y', (int)$result[$i]['enddate']));
            $objectCounter->appendChild($expireDate);
          }
        }
    	$statsPageXML->appendChild($ongoingAuctionObjects);

    	$soldObjects=$xml->createElement("soldObjects");
    	$result = $this->sqlInterrogation->getObjectsSold($_SESSION['idUser']);
    	if(count($result)==0){
          $soldObjects->setAttribute("nrObjects", 0);
        }else{
          $soldObjects->setAttribute("nrObjects", count($result));
          for ($i = 0; $i < count($result); $i++) {
            $objectCounter=$xml->createElement("object");
            $objectCounter->setAttribute("id",$result[$i]['idObject']);
            $soldObjects->appendChild($objectCounter);

            $name=$xml->createElement("name");
            $name->setAttribute("value",$result[$i]['name']);
            $objectCounter->appendChild($name);

            $category=$xml->createElement("category");
            $category->setAttribute("value",$result[$i]['category']);
            $objectCounter->appendChild($category);

            $subcategory=$xml->createElement("subcategory");
            $subcategory->setAttribute("value",$result[$i]['subcategory']);
            $objectCounter->appendChild($subcategory);

            $price=$xml->createElement("price");
            $price->setAttribute("value",$result[$i]['price']);
            $objectCounter->appendChild($price);

            $expireDate=$xml->createElement("date");
            $expireDate->setAttribute("value",date('d-M-Y', (int)$result[$i]['enddate']));
            $objectCounter->appendChild($expireDate);
          }
        }
    	$statsPageXML->appendChild($soldObjects);

  	// echo "<xmp>".$xml->saveXML()."</xmp>";
    $filename = $this->idUser.'_stats.xml';
    $xml->saveXML();
    $xml->save($filename);
    return $filename;
  }

  /* generate json file
  * returns file name
  **/
  function getJSON(){

    $sum = array('total' => $this->sqlInterrogation->getSumTotal(),
      'lastWeek: ' => $this->sqlInterrogation->getSumLastWeek(),
      'lastDay: ' => $this->sqlInterrogation->getSumLastDay(),
      'lastMonth: ' => $this->sqlInterrogation->getSumLastMonth()
    );

    $json = array('sum' => $sum,
      'objectsFavourites' => $this->sqlInterrogation->getObjectsFavourites($_SESSION['idUser']),
      'objectsForSale' => $this->sqlInterrogation->getObjectsForSale($_SESSION['idUser']),
      'objectsPurchased' => $this->sqlInterrogation->getObjectsPurchased($_SESSION['idUser']),
      'objectsOngoingAuction' => $this->sqlInterrogation->getObjectsOngoingAuction($_SESSION['idUser']),
      'objectsSold' => $this->sqlInterrogation->getObjectsSold($_SESSION['idUser'])
    );

    $aux = $this->idUser;
    $filename = $this->idUser.'_stats.json';
    $fp = fopen($filename, 'w');
    fwrite($fp, json_encode($json));
    fclose($fp);
    return $filename;
  }

}
?>
