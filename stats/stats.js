
function openObjectDetails(id){
  // http things
  goTo('../object-details/object-details.php?' + id);
}

function openAddObject(){
  // http things
  goTo('../add-object/add-object.php');
}

function openStats(){
  // http things
  goTo('../stats/stats.php');
}

function goTo(path){
  window.location.href = path;
}
