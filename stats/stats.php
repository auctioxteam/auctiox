<?php
  include '../sqlInterrogations.php';
  include './generateFiles.php';
  session_start();
  $sqlInterrogation = new SQLInterrogation();
  $generateFiles = new GenerateFiles($_SESSION['idUser'], $_SESSION['firstname'].' '.$_SESSION['lastname']);
  $loginPageUrl = 'http://'.$_SERVER['HTTP_HOST'].'/app/login/signin.php';

  if (!isset($_SESSION['isLogedIn']) || $_SESSION['isLogedIn'] !== 'true'){
    header("Location: $loginPageUrl");
    exit();
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['signout'])){
      $_SESSION['isLogedIn'] = 'false';
      header("Location: $loginPageUrl");
      exit();
    }
  }
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>AuctioX</title>
    <link rel="stylesheet" type="text/css" href="../core/_buttons.css">
    <link rel="stylesheet" type="text/css" href="../core/_inputs.css">
    <link rel="stylesheet" type="text/css" href="../core/_util.css">
    <link rel="stylesheet" type="text/css" href="../content/_content.css">
    <link rel="stylesheet" type="text/css" href="./_stats.css">
    <script type="text/javascript" src="stats.js"></script>
  </head>
  <body>
    <div class="main-page stats-page">

      <header>
        <div class="title" onclick="goTo('../content/content.php')">
          AuctioX
        </div>
        <div class="signout">
          Hey, <span class="user"><?php echo $_SESSION['firstname'].' '.$_SESSION['lastname']; ?></span> <form action="<?php $_SERVER['PHP_SELF']?>" method="post"><button type="submit" name="signout" class="button -small -standard" name="button">Sign Out</button></form>
        </div>
        <div class="user-name">
          <?php echo $_SESSION['firstname'].' '.$_SESSION['lastname']; ?> - stats
        </div>
      </header>

      <div class="user-stats">
        <div class="column">
          <?php
            $amountDay = $sqlInterrogation->getSumLastDay();
            $amountWeek = $sqlInterrogation->getSumLastWeek();
            $amountMonth = $sqlInterrogation->getSumLastMonth();
            $amountTotal = $sqlInterrogation->getSumTotal();
            ?>
          <div class="title">Total amount: <?php echo $amountTotal ?>$</div>
          <div class="item">Last day:  <?php echo $amountDay ?>$</div>
          <div class="item">Last week:  <?php echo $amountWeek ?>$</div>
          <div class="item">Last month:  <?php echo $amountMonth ?>$</div>
        </div>
        <div class="column">
          <div class="title">Download data as:</div>
          <div class="item download-button">
            <a href="<?php echo $generateFiles->getJSON()?>" download="stats.json">
              <button class="button -small -auctiox" type="button" name="button" onclick="">
                JSON
              </button>
            </a>
          </div>
          <div class="item download-button">
            <a href="<?php echo $generateFiles->getXML()?>" download="stats.xml">
              <button class="button -small -auctiox" type="button" name="button" onclick="">
                XML
              </button>
            </a>
          </div>
          <div class="item download-button">
            <a href="<?php echo $generateFiles->getPDF()?>" download="stats.pdf">
              <button class="button -small -auctiox" type="button" name="button" onclick="">
                PDF
              </button>
            </a>
          </div>
        </div>
      </div>
      <section>
        <?php
          $resultFavourites = $sqlInterrogation->getObjectsFavourites($_SESSION['idUser']);
          $arrayLenght = count($resultFavourites);

          echo '<div class="">';
          echo '   Favourites objects: '.$arrayLenght;
          echo '</div>';
          echo '<div class="objects-list">';
          for ($i=0; $i < $arrayLenght; $i++) {
            echo '<div class="object" onClick="openObjectDetails(' . $resultFavourites[$i]['idObject'] .   ')">';
            echo '  <div class="left">';
            echo '    <div class="img">';
            echo '      <img src=' . 'http://' . $_SERVER['SERVER_NAME'] . '/app' .$resultFavourites[$i]['image'] . ' alt=""/>';
            echo '    </div>';
            echo '    <div class="details">';
            echo '      <div class="up">';
            echo '        <div class="title">';
            echo '          ' . $resultFavourites[$i]['name'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '      <div class="down">';
            echo '        <div class="category">';
            echo '          ' . $resultFavourites[$i]['category'] . '';
            echo '        </div>';
            echo '        >>';
            echo '        <div class="subcategory">';
            echo '          ' . $resultFavourites[$i]['subcategory'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '    </div>';
            echo '  </div>';
            echo '  <div class="right">';
            echo '    <div class="price">';
            echo '          ' . $resultFavourites[$i]['price'] . '$';
            echo '    </div>';
            echo '    <div class="dimensions">';
            echo '       ' . date ( 'd-M-Y', (int)$resultFavourites[$i]['enddate'] ) . '';
            echo '    </div>';
            echo '  </div>';
            echo '</div>';
          }
          echo '</div>';
        ?>
      </section>
      <section>
        <?php
          $resultForSale = $sqlInterrogation->getObjectsForSale($_SESSION['idUser']);
          $arrayLenght = count($resultForSale);

          echo '<div class="">';
          echo    'Objects for sale: '.$arrayLenght;
          echo '</div>';
          echo '<div class="objects-list">';
          for ($i=0; $i < $arrayLenght; $i++) {
            echo '<div class="object" onClick="openObjectDetails(' . $resultForSale[$i]['idObject'] .   ')">';
            echo '  <div class="left">';
            echo '    <div class="img">';
            echo '      <img src=' . 'http://' . $_SERVER['SERVER_NAME'] . '/app' .$resultForSale[$i]['image'] . ' alt=""/>';
            echo '    </div>';
            echo '    <div class="details">';
            echo '      <div class="up">';
            echo '        <div class="title">';
            echo '          ' . $resultForSale[$i]['name'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '      <div class="down">';
            echo '        <div class="category">';
            echo '          ' . $resultForSale[$i]['category'] . '';
            echo '        </div>';
            echo '        >>';
            echo '        <div class="subcategory">';
            echo '          ' . $resultForSale[$i]['subcategory'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '    </div>';
            echo '  </div>';
            echo '  <div class="right">';
            echo '    <div class="price">';
            echo '          ' . $resultForSale[$i]['price'] . '$';
            echo '    </div>';
            echo '    <div class="dimensions">';
            echo '       ' . date ( 'd-M-Y', (int)$resultForSale[$i]['enddate'] ) . '';
            echo '    </div>';
            echo '  </div>';
            echo '</div>';
          }
          echo '</div>';
        ?>
      </section>
      <section>
        <?php
            $resultPurchased = $sqlInterrogation->getObjectsPurchased($_SESSION['idUser']);
            $arrayLenght = count($resultPurchased);

            echo '<div class="">';
            echo '   Objects purchased: '.$arrayLenght;
            echo '</div>';
            echo '<div class="objects-list">';
            for ($i=0; $i < $arrayLenght; $i++) {
              echo '<div class="object" onClick="openObjectDetails(' . $resultPurchased[$i]['idObject'] .   ')">';
              echo '  <div class="left">';
              echo '    <div class="img">';
              echo '      <img src=' . 'http://' . $_SERVER['SERVER_NAME'] . '/app' .$resultPurchased[$i]['image'] . ' alt=""/>';
              echo '    </div>';
              echo '    <div class="details">';
              echo '      <div class="up">';
              echo '        <div class="title">';
              echo '          ' . $resultPurchased[$i]['name'] . '';
              echo '        </div>';
              echo '      </div>';
              echo '      <div class="down">';
              echo '        <div class="category">';
              echo '          ' . $resultPurchased[$i]['category'] . '';
              echo '        </div>';
              echo '        >>';
              echo '        <div class="subcategory">';
              echo '          ' . $resultPurchased[$i]['subcategory'] . '';
              echo '        </div>';
              echo '      </div>';
              echo '    </div>';
              echo '  </div>';
              echo '  <div class="right">';
              echo '    <div class="price">';
              echo '          ' . $resultPurchased[$i]['price'] . '$';
              echo '    </div>';
              echo '    <div class="dimensions">';
              echo '       ' . date ( 'd-M-Y', (int)$resultPurchased[$i]['enddate'] ) . '';
              echo '    </div>';
              echo '  </div>';
              echo '</div>';
            }
            echo '</div>';
        ?>
      </section>
      <section>
        <?php
          $resultOngoingAuction = $sqlInterrogation->getObjectsOngoingAuction($_SESSION['idUser']);
          $arrayLenght = count($resultOngoingAuction);

          echo '<div class="">';
          echo '   Ongoing auction: '.$arrayLenght;
          echo '</div>';
          echo '<div class="objects-list">';
          for ($i=0; $i < $arrayLenght; $i++) {
            echo '<div class="object" onClick="openObjectDetails(' . $resultOngoingAuction[$i]['idObject'] .   ')">';
            echo '  <div class="left">';
            echo '    <div class="img">';
            echo '      <img src=' . 'http://' . $_SERVER['SERVER_NAME'] . '/app' .$resultOngoingAuction[$i]['image'] . ' alt=""/>';
            echo '    </div>';
            echo '    <div class="details">';
            echo '      <div class="up">';
            echo '        <div class="title">';
            echo '          ' . $resultOngoingAuction[$i]['name'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '      <div class="down">';
            echo '        <div class="category">';
            echo '          ' . $resultOngoingAuction[$i]['category'] . '';
            echo '        </div>';
            echo '        >>';
            echo '        <div class="subcategory">';
            echo '          ' . $resultOngoingAuction[$i]['subcategory'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '    </div>';
            echo '  </div>';
            echo '  <div class="right">';
            echo '    <div class="price">';
            echo '          ' . $resultOngoingAuction[$i]['price'] . '$';
            echo '    </div>';
            echo '    <div class="dimensions">';
            echo '       ' . date ( 'd-M-Y', (int)$resultOngoingAuction[$i]['enddate'] ) . '';
            echo '    </div>';
            echo '  </div>';
            echo '</div>';
          }
          echo '</div>';
        ?>
      </section>
      <section>
        <?php
          $resultSold = $sqlInterrogation->getObjectsSold($_SESSION['idUser']);
          $arrayLenght = count($resultSold);

          echo '<div class="">';
          echo '   Sold items: '.$arrayLenght;
          echo '</div>';
          echo '<div class="objects-list">';
          for ($i=0; $i < $arrayLenght; $i++) {
            echo '<div class="object" onClick="openObjectDetails(' . $resultSold[$i]['idObject'] .   ')">';
            echo '  <div class="left">';
            echo '    <div class="img">';
            echo '      <img src=' . 'http://' . $_SERVER['SERVER_NAME'] . '/app' .$resultSold[$i]['image'] . ' alt=""/>';
            echo '    </div>';
            echo '    <div class="details">';
            echo '      <div class="up">';
            echo '        <div class="title">';
            echo '          ' . $resultSold[$i]['name'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '      <div class="down">';
            echo '        <div class="category">';
            echo '          ' . $resultSold[$i]['category'] . '';
            echo '        </div>';
            echo '        >>';
            echo '        <div class="subcategory">';
            echo '          ' . $resultSold[$i]['subcategory'] . '';
            echo '        </div>';
            echo '      </div>';
            echo '    </div>';
            echo '  </div>';
            echo '  <div class="right">';
            echo '    <div class="price">';
            echo '          ' . $resultSold[$i]['price'] . '$';
            echo '    </div>';
            echo '    <div class="dimensions">';
            echo '       ' . date ( 'd-M-Y', (int)$resultSold[$i]['enddate'] ) . '';
            echo '    </div>';
            echo '  </div>';
            echo '</div>';
          }
          echo '</div>';
        ?>
      </section>

      <footer>
        <div class="column">
          <div class="title">Authors</div>
          <a href="#">Amariucai Ioana</a>
          <a href="#">Balan Nicoleta Anca</a>
          <a href="#">Covrig Catalin</a>
          <a href="#">Osoeanu Florentina</a>
        </div>

        <div class="column">
          <div class="title">Teachers</div>
          <a href="#">Sabin Buraga</a>
          <a href="#">Coman Alexandru</a>
        </div>

        <div class="column">
          <div class="title">Project Related</div>
          <a href="https://bitbucket.org/auctioxteam/auctiox/overview" target="_blank">Project link</a>
          <a href="https://bitbucket.org/auctioxteam/auctiox/src" target="_blank">Source code</a>
        </div>
      </footer>
    </div>
  </body>
</html>
