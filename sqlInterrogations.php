<?php
include 'connectionHandler.php';
include 'AddEntryToFeed.php';

class SQLInterrogation{

  function SQLInterrogation(){
    $this->NewConnection = new Connection();
  }

  function addUser($email, $firstname, $lastname, $passwd, $city, $phone, $zip){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "INSERT INTO user (email, firstname, lastname, password, city, phone, zip)
              VALUES ( '$email', '$firstname', '$lastname', '$passwd', '$city', $phone, $zip)";
      $stmt = $conn->prepare($sql);
      $result = $stmt->execute();
      // echo "New record created successfully";
    }
    catch(PDOException $e){
      $this->NewConnection->closeConnectionDB();
      return $e->getMessage();
    }
    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  function addObject($category, $subcategory, $degreeOfWearing, $name, $price, $description, $image){
    $conn = $this->NewConnection->makeConnectionDB();
    $addEntryToFeed = new AddEntryToFeed();

    $idUser = $_SESSION['idUser'];
    $TIME_GAP = 60 * 60 * 24 * 30;  //one month
    $deadline = time() + $TIME_GAP;
    $startline = time();
    $PATH = '/assets/img/image/';

    $imagetmp = $image['tmp_name'];
    $imagename = $image['name'];
    $imagePath = $PATH . $imagename;

    $imageFileType = pathinfo($imagename, PATHINFO_EXTENSION);
    // print_r($imageFileType);

    if($imageFileType === 'jpg'){
      // move to folder
      move_uploaded_file($imagetmp, '..' . $imagePath);

      try {
        // in DB save details + image path ( not image itself)
        $sql = "INSERT INTO objects ( idSeller, category, subcategory, degreeOfWearing, name, startDate, endDate, price, description, image)
                VALUES ( $idUser, '$category', '$subcategory', '$degreeOfWearing', '$name', '$startline', '$deadline', $price, '$description', '$imagePath')";
        // $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $result = $stmt->execute();

        $object = $this->getObjectId($startline);

        // update the feed - WIP
        $addEntryToFeed->addEntry($object['idObject'], $name, $startline, $startline, $object['firstname'] . ' ' . $object['lastname'], $category, $subcategory, $price, $deadline, $imagePath);
      }
      catch(PDOException $e){
        echo $e->getMessage();
        return $e->getMessage();
      }
    }else{
      return 'Image extention should be JPG!!!';
    }

    $this->NewConnection->closeConnectionDB();
    return true;
  }

  function addNewBid($idObject, $price, $time){
    $conn = $this->NewConnection->makeConnectionDB();
    $addEntryToFeed = new AddEntryToFeed();
    $idBuyer = (int)$_SESSION['idUser'];

    try {
      $sql = "INSERT INTO bid ( idObject, idBuyer, price, time)
              VALUES ( $idObject, $idBuyer, $price, '$time')";
      // $conn->exec($sql);
      $stmt = $conn->prepare($sql);
      $result = $stmt->execute();

      // update the object
      $this->setObjectAfterBid($idObject, $idBuyer, $price);

      // WIP update - feed
      $addEntryToFeed->updateEntryPrice($idObject, $price, $time);
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      $this->NewConnection->closeConnectionDB();
      return false;
    }

    $this->NewConnection->closeConnectionDB();
    return true;
  }

  function addNewFavourite($idObject){
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];
    $time = ''.time();

    try {
      $sql = "INSERT INTO favourite ( idObject, idUser, isFavourite, updated)
              VALUES ( $idObject, $idUser, true, '$time')";

      $stmt = $conn->prepare($sql);
      $result = $stmt->execute();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      $this->NewConnection->closeConnectionDB();
      return $result;
    }

    $this->NewConnection->closeConnectionDB();
    return true;
  }

  function addNewReport($idObject){
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];
    $time = ''.time();

    try {
      $sql = "INSERT INTO report ( idObject, idUser, isReported, updated)
              VALUES ( $idObject, $idUser, true, '$time')";

      $stmt = $conn->prepare($sql);
      $result = $stmt->execute();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      $this->NewConnection->closeConnectionDB();
      return $result;
    }

    $this->NewConnection->closeConnectionDB();
    return true;
  }

  function getObjectId($startDate){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT idObject, user.firstname, user.lastname
                FROM objects, user
                WHERE startDate='$startDate'
                AND objects.idSeller = user.id";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  function setObjectAfterBid($idObject, $idBuyer, $price){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "UPDATE objects
              SET idBuyer=$idBuyer, price=$price
              WHERE idObject=$idObject";

      $stmt = $conn->prepare($sql);
      $stmt->execute();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      return false;
    }

    $this->NewConnection->closeConnectionDB();
    return true;
  }

  function setFavourite($id, $isFavourite){
    // echo $isFavourite . ' favourite';
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];
    $time = ''.time();

    try {
      $sql = "UPDATE favourite
              SET isFavourite = '$isFavourite', updated = '$time'
              WHERE id = $id
                AND idUser = $idUser";

      $stmt = $conn->prepare($sql);
      $result = $stmt->execute();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      return false;
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // update Object as reported
  function updateObjectAsReported($idObject){
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];
    $time = ''.time();

    try {
      $sql = "UPDATE objects
              SET isReported = true
              WHERE idObject = $idObject";

      $stmt = $conn->prepare($sql);
      $result = $stmt->execute();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      return false;
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  function getUser($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT user.firstname,
                     user.lastname,
                     user.city,
                     user.zip,
                     user.phone
               FROM user
               WHERE user.id='$id'";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      return $result;
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }
  }

  function getUserByEmail($email){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT id, password, firstname, lastname, email
                FROM user
                WHERE email='$email'";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      return $result;
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
  }

  function getObjectDetails($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT objects.name,
                     objects.category,
                     objects.subcategory,
                     objects.price,
                     objects.description,
                     objects.image,
                     objects.endDate,
                     objects.isReported,
                     user.firstname,
                     user.lastname,
                     user.city,
                     user.zip,
                     user.phone
               FROM user,objects
               WHERE objects.idObject='$id'
                  AND objects.idSeller=user.id";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      return $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  function getBidsNumber($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT count(bid.id)
               FROM bid
               WHERE bid.idObject='$id'";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e){
      echo $stmt . "<br>" . $e->getMessage();
      return '';
    }

    $this->NewConnection->closeConnectionDB();
    return $result['count(bid.id)'];
  }

  function getObjectDetailsWithBids($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT objects.name,
                     objects.category,
                     objects.subcategory,
                     objects.description,
                     objects.image,
                     objects.endDate,
                     objects.isReported,
                     user.firstname,
                     user.lastname,
                     user.city,
                     user.zip,
                     user.phone,
                     bid.idBuyer,
                     MAX(bid.price)
               FROM user, objects, bid
               WHERE objects.idObject='$id'
                  AND objects.idObject=bid.idObject
                  AND objects.idSeller=user.id";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // get details about buyer for object id
  function getBuyers($idObject){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT DISTINCT user.email, user.firstname, user.lastname
              FROM bid, user
              WHERE bid.idObject=$idObject
                AND bid.idBuyer=user.id";

      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // get details about favourites -> object
  function getFavouriteByUser($idObject){
    $idUser = (int)$_SESSION['idUser'];
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT *
              FROM favourite
              WHERE favourite.idObject=$idObject
                AND favourite.idUser=$idUser";

      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // get details about buyer for object id
  function getReport($idObject){
    $idUser = (int)$_SESSION['idUser'];
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT *
              FROM report
              WHERE report.idObject=$idObject
                AND report.idUser=$idUser";

      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // get details about buyer for object id
  function getReportNr($idObject){
    $idUser = (int)$_SESSION['idUser'];
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT COUNT(*) as nr
              FROM report
              WHERE report.idObject=$idObject";

      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return (int)$result[0]['nr'];
  }


  // amount money total
  function getSumTotal(){
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];

    try {
      $sql ="SELECT SUM(objects.price) as price
             FROM objects
             WHERE objects.idSeller=$idUser
              AND FROM_UNIXTIME(endDate) < CURDATE()";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return (int)$result[0]['price'];
  }

  // amount money last day
  function getSumLastDay(){
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];

    try {
      $sql ="SELECT SUM(objects.price) as price
             FROM objects
             WHERE objects.idSeller=$idUser
              AND FROM_UNIXTIME(endDate) BETWEEN date_sub(CURDATE(),INTERVAL 1 DAY) and CURDATE()";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
      // print_r($result);
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
      return $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return (int)$result[0]['price'];
  }

    // amount money last week
  function getSumLastWeek(){
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];

    try {
      $sql ="SELECT SUM(objects.price) as price
             FROM objects
             WHERE objects.idSeller=$idUser
              AND FROM_UNIXTIME(endDate) BETWEEN date_sub(CURDATE(),INTERVAL 1 WEEK) and CURDATE()";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return (int)$result[0]['price'];
  }

    // amount money last month
  function getSumLastMonth(){
    $conn = $this->NewConnection->makeConnectionDB();
    $idUser = (int)$_SESSION['idUser'];

    try {
      $sql ="SELECT SUM(objects.price) as price
             FROM objects
             WHERE objects.idSeller=$idUser
              AND FROM_UNIXTIME(endDate) BETWEEN date_sub(CURDATE(),INTERVAL 1 MONTH) and CURDATE()";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return (int)$result[0]['price'];
  }

  // get favourite objects details
  function getObjectsFavourites(){
    $idUser = (int)$_SESSION['idUser'];
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image
              FROM favourite, objects
              WHERE favourite.idObject=objects.idObject
                AND favourite.idUser=$idUser
                AND favourite.isFavourite = (1)";

      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // obiects for sale
  function getObjectsForSale($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image
              FROM objects
              WHERE FROM_UNIXTIME(endDate) > CURDATE() AND objects.idSeller=$id";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // purchased obiects
  function getObjectsPurchased($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image
              FROM objects
              WHERE FROM_UNIXTIME(endDate) < CURDATE() AND objects.idBuyer=$id";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // Ongoing Auction
  function getObjectsOngoingAuction($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image
              FROM objects
              WHERE FROM_UNIXTIME(endDate) > CURDATE()
                AND objects.idObject IN ( SELECT DISTINCT idObject
                                          FROM bid
                                          WHERE bid.idBuyer=$id
                                        )";

      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }

  // sold objects
  function getObjectsSold($id){
    $conn = $this->NewConnection->makeConnectionDB();

    try {
      $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image
              FROM objects
              WHERE FROM_UNIXTIME(endDate) < CURDATE()
                AND objects.idSeller=$id";

      $stmt = $conn->prepare($sql);
      $stmt->execute();

      $result = $stmt->fetchAll();
    }
    catch(PDOException $e){
      echo $sql . "<br>" . $e->getMessage();
    }

    $this->NewConnection->closeConnectionDB();
    return $result;
  }



    function filter($owner, $mytext, $city, $category,$sortByPrice, $sortByDegreeOfWear, $sortByDeadline, $sortIntervalFrom, $sortIntervalTo){
      $idUser = (int)$_SESSION['idUser'];

      if($owner == 'mine'){
      $conn = $this->NewConnection->makeConnectionDB();
     /* echo $sortByPrice;
      echo $sortIntervalFrom;*/


      try {
        $searchSql='';
        $searchArray = explode(' ', $mytext);
        foreach ($searchArray as $value) {
          $searchSql .= " AND (objects.description LIKE '%" . $value . "%' OR objects.category LIKE '%" . $value . "%' OR objects.subcategory LIKE '%" . $value . "%' )";
        }
        // echo $searchSql;

        $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image, objects.degreeOfWearing
                FROM objects, user
                WHERE objects.idSeller LIKE '$idUser' AND objects.idSeller = user.id";
        if($city!='') $sql .= " AND user.city LIKE '$city'";
        if(count($searchArray)>0) $sql .= $searchSql;
        if($category!='') $sql .= " AND objects.category LIKE '$category'";
        if($sortIntervalFrom != '') $sql .= " AND objects.price > $sortIntervalFrom";
        if($sortIntervalTo != '') $sql .= " AND objects.price < $sortIntervalTo";
        if($sortByPrice == 'ascending') $sql .= " ORDER BY objects.price ASC";
        if($sortByPrice == 'descending') $sql .= " ORDER BY objects.price DESC";
        if($sortByDegreeOfWear == 'new') $sql .= " ORDER BY objects.degreeOfWearing ASC";
        if($sortByDegreeOfWear == 'used') $sql .= " ORDER BY objects.degreeOfWearing DESC";
        if($sortByDeadline == 'true') $sql .= " ORDER BY objects.enddate ASC";

        // echo $sql;

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
      }
      catch(PDOException $e){
        echo $sql . "<br>" . $e->getMessage();
      }

      $this->NewConnection->closeConnectionDB();
      return $result;
    }
    elseif ($owner=='others') {
      $conn = $this->NewConnection->makeConnectionDB();

      try {
        $searchSql='';
        $searchArray = explode(' ', $mytext);
        foreach ($searchArray as $value) {
          $searchSql .= " AND (objects.description LIKE '%" . $value . "%' OR objects.category LIKE '%" . $value . "%' OR objects.subcategory LIKE '%" . $value . "%' )";
        }

        $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image, objects.degreeOfWearing
                FROM objects, user
               WHERE objects.idSeller != '$idUser' AND objects.idSeller = user.id";
        if($city!='') $sql .= " AND user.city LIKE '$city'";
        if(count($searchArray)>0) $sql .= $searchSql;
        if($category!='') $sql .= " AND objects.category LIKE '$category'";
        if($sortIntervalFrom != '') $sql .= " AND objects.price > $sortIntervalFrom";
        if($sortIntervalTo != '') $sql .= " AND objects.price < $sortIntervalTo";
        if($sortByPrice == 'ascending') $sql .= " ORDER BY objects.price ASC";
        if($sortByPrice == 'descending') $sql .= " ORDER BY objects.price DESC";
        if($sortByDegreeOfWear == 'new') $sql .= " ORDER BY objects.degreeOfWearing ASC";
        if($sortByDegreeOfWear == 'used') $sql .= " ORDER BY objects.degreeOfWearing DESC";
        if($sortByDeadline == 'true') $sql .= " ORDER BY objects.enddate ASC";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
      }
      catch(PDOException $e){
        echo $sql . "<br>" . $e->getMessage();
      }

      $this->NewConnection->closeConnectionDB();
      return $result;
    }
     elseif ($owner=='all') {
      $conn = $this->NewConnection->makeConnectionDB();

      try {
        $searchSql='';
        $searchArray = explode(' ', $mytext);
        foreach ($searchArray as $value) {
          $searchSql .= " AND (objects.description LIKE '%" . $value . "%' OR objects.category LIKE '%" . $value . "%' OR objects.subcategory LIKE '%" . $value . "%' )";
        }

        $sql = "SELECT objects.idObject, objects.name, objects.startDate, objects.category, objects.subcategory, objects.price, objects.enddate, objects.image, objects.degreeOfWearing
                FROM objects, user
                WHERE objects.idSeller = user.id";
        if($city!='') $sql .= " AND user.city LIKE '$city'";
        if(count($searchArray)>0) $sql .= $searchSql;
        if($category!='') $sql .= " AND objects.category LIKE '$category'";
        if($sortIntervalFrom != '') $sql .= " AND objects.price > $sortIntervalFrom";
        if($sortIntervalTo != '') $sql .= " AND objects.price < $sortIntervalTo";
        if($sortByPrice == 'ascending') $sql .= " ORDER BY objects.price ASC";
        if($sortByPrice == 'descending') $sql .= " ORDER BY objects.price DESC";
        if($sortByDegreeOfWear == 'new') $sql .= " ORDER BY objects.degreeOfWearing ASC";
        if($sortByDegreeOfWear == 'used') $sql .= " ORDER BY objects.degreeOfWearing DESC";
        if($sortByDeadline == 'true') $sql .= " ORDER BY objects.enddate ASC";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();
      }
      catch(PDOException $e){
        echo $sql . "<br>" . $e->getMessage();
      }

      $this->NewConnection->closeConnectionDB();
      return $result;
    }
  }
}

?>
