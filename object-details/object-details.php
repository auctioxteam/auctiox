<?php
  $loginPageUrl = 'http://'.$_SERVER['HTTP_HOST'].'/app/login/signin.php';
  $mailsMsg='';

  session_start();
  if (!isset($_SESSION['isLogedIn']) || $_SESSION['isLogedIn'] !== 'true'){
    header("Location: $loginPageUrl");
    exit();
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['signout'])){
      $_SESSION['isLogedIn'] = 'false';
      header("Location: $loginPageUrl");
      exit();
    }
  }
?>
<?php
include '../sqlInterrogations.php';
include '../emailHandler.php';

$currentUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$currentObjectId = parse_url($currentUrl, PHP_URL_QUERY);
$sqlInterrogation = new SQLInterrogation();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST['submitBid'])){
       $result = $sqlInterrogation->addNewBid(htmlspecialchars($currentObjectId),
                                              htmlspecialchars($_POST['newBid']),
                                              htmlspecialchars(time().'')
                                              );
       if($result !== true){
          $errorMsg = $result;
        }else{
          $emailHandler = new EmailHandler();
          $isSent = $emailHandler->sendEmailsNewBid($sqlInterrogation->getBuyers($currentObjectId), $sqlInterrogation->getFavouriteByUser($currentObjectId));

          if($isSent){
            $mailsMsg = 'You made a new offer. The owner has been announced by email.';
          }else{
            $mailsMsg = 'We could not send the emails.';
          }
        }
  }
}

// add/remove from favourite
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST['submitReport'])){

    $result = $sqlInterrogation->getReport($currentObjectId);

    if(count($result) === 0){
      $result = $sqlInterrogation->addNewReport(htmlspecialchars($currentObjectId));
      if($result){
        $result = $sqlInterrogation->getReportNr($currentObjectId);
        if($result >= 2){   // object is declared as reported only after a number of reports
          $sqlInterrogation->updateObjectAsReported($currentObjectId);
        }
      }else {
        print_r(0);
      }
    }else if($result[0]['isReported'] !== '1'){
      // reported -> true
    //   $result = $sqlInterrogation->setReport($result[0]['id'], true);
    // }else{
    //   // reported -> false
    //   $result = $sqlInterrogation->setReport($result[0]['id'], false );
    }
  }
}

$report = $sqlInterrogation->getReport($currentObjectId);

if(count($report) === 0){
  $reportDisable = 0;
}else if($report[0]['isReported'] !== '1'){
  $reportDisable = 0;
}else{
  $reportDisable = 1;
}

// add/remove from favourite
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST['submitFavourite'])){

    $result = $sqlInterrogation->getFavouriteByUser($currentObjectId);

    if(count($result) === 0){
      $result = $sqlInterrogation->addNewFavourite(htmlspecialchars($currentObjectId));
    }else if($result[0]['isFavourite'] !== '1'){
      // print_r('false');
      // updateIt with true
      $result = $sqlInterrogation->setFavourite($result[0]['id'], true);
      // $isFavourite = '';
    }else{
      // print_r('true');
      // updateIt with false
      $result = $sqlInterrogation->setFavourite($result[0]['id'], false );
      // $isFavourite = '-star';
    }
  }
}

// set favourite attributtes
  $result = $sqlInterrogation->getFavouriteByUser($currentObjectId);

  if(count($result) === 0){
    $isFavourite = '';
  }else if($result[0]['isFavourite'] !== '1'){
    $isFavourite = '';
  }else{
    $isFavourite = '-star';
  }



//set data on page
if($sqlInterrogation->getBidsNumber($currentObjectId) == 0){
  $result = $sqlInterrogation->getObjectDetails($currentObjectId);
  $objectTitle = $result['name'];
  $objectImage = "http://" . $_SERVER['SERVER_NAME'] . '/app' . $result['image'] . "";
  $objectCategory = $result['category'];
  $objectSubcategory = $result['subcategory'];
  $objectDeadline = date( 'd-M-Y', (int)$result['endDate']);
  $objectPrice = $result['price'];
  $objectDescription = $result['description'];
  $ownerName = $result['firstname'].' '.$result['lastname'];
  $ownerPhone = $result['phone'];
  $ownerZip = $result['zip'];
  $ownerCity = $result['city'];
  $isReported = $result['isReported'];
  if(time() < (int)$result['endDate']){
    $deadlineOver=0;
  }else{
    $deadlineOver=1;
  }

  $buyerName = '-';
}else{
  $result = $sqlInterrogation->getObjectDetailsWithBids($currentObjectId);
  $objectTitle = $result['name'];
  $objectImage = "http://" . $_SERVER['SERVER_NAME'] . '/app' . $result['image'] . "";
  $objectCategory = $result['category'];
  $objectSubcategory = $result['subcategory'];
  $objectDeadline = date( 'd-M-Y', (int)$result['endDate']);
  $objectPrice = $result['MAX(bid.price)'];
  $objectDescription = $result['description'];
  $ownerName = $result['firstname'].' '.$result['lastname'];
  $ownerPhone = $result['phone'];
  $ownerZip = $result['zip'];
  $ownerCity = $result['city'];
  $isReported = $result['isReported'];
  if(time() < (int)$result['endDate']){
    $deadlineOver=0;
  }else{
    $deadlineOver=1;
  }

  $result = $sqlInterrogation->getUser($result['idBuyer']);
  $buyerName = $result['firstname'].' '.$result['lastname'];
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AuctioX</title>
    <link rel="stylesheet" type="text/css" href="../core/_buttons.css">
    <link rel="stylesheet" type="text/css" href="../core/_inputs.css">
    <link rel="stylesheet" type="text/css" href="../core/_util.css">
    <link rel="stylesheet" type="text/css" href="../content/_content.css">
    <link rel="stylesheet" type="text/css" href="./_object-details.css">
    <script type="text/javascript" src="object-details.js"></script>
  </head>
  <body>
    <div class="main-page object-details">
      <header>
        <div class="title" onclick="goTo('../content/content.php')">
          AuctioX
        </div>
        <div class="announcement">
          <?php echo $mailsMsg; ?>
        </div>
        <div class="signout">
          Hey, <span class="user"><?php echo $_SESSION['firstname'].' '.$_SESSION['lastname']; ?></span> <form action="<?php $_SERVER['PHP_SELF']?>" method="post"><button type="submit" name="signout" class="button -small -standard" name="button">Sign Out</button></form>
        </div>
        <div class="image-wrapper">
          <div class="object-image">
            <img src=<?php echo $objectImage?>>
          </div>
        </div>
        <div class="object-title">
          <?php echo $objectTitle ?>
        </div>

        <div class="new-bid">
          <form  action="<?php $_SERVER['PHP_SELF']?>" method="post" class="new-bid">
            <div class="price">Last offer: <?php echo $objectPrice?>$</div>
            <input type="number" name="newBid" placeholder="Make a new offer" class="input -standard -small" value=<?php echo $objectPrice+1?> min=<?php echo $objectPrice+1?> <?php echo $isReported==='1' || $deadlineOver===1 ? 'disabled' : '';?> required>
            <button class="button -icon -small -auctiox" type="submit" name="submitBid" title="Make a new offer for this object" <?php echo $isReported==='1' || $deadlineOver===1 ? 'disabled' : '';?>>
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#bid"></use>
              </svg>
            </button>
          </form>
          <form  action="<?php $_SERVER['PHP_SELF']?>" method="post" class="favourite">
            <button class="button -icon -small -standard <?php echo $isFavourite?>" title="Add object to favourites" type="submit" name="submitFavourite">
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#favourite"></use>
              </svg>
            </button>
          </form>
          <form  action="<?php $_SERVER['PHP_SELF']?>" method="post" class="report">
            <button class="button -small -standard" title="Report this object" type="submit" name="submitReport" <?php echo $reportDisable===1 ? 'disabled' : '';?>>
              <?php echo $reportDisable===1 ? 'You report this object' : 'Report the object';?>
            </button>
          </form>
        </div>
      </header>

      <div class="object-content">
         <?php echo $isReported==='1' ? '<div class="reported">This object has been reported. You can not make a new offer anymore!</div>' : '';?>>
        <div class="object-dates">
          <div class="column">
            <div class="item">Title: <?php echo $objectTitle?></div>
            <div class="item">Category: <?php echo $objectCategory?></div>
            <div class="item">Subcategory: <?php echo $objectSubcategory?></div>
          </div>
          <div class="column">
            <div class="item">Deadline: <?php echo $objectDeadline?></div>
            <div class="item">Last Bid: <?php echo $objectPrice?></div>
            <div class="item">Last Bid: <?php echo $buyerName?></div>
          </div>
          <div class="column">
            <div class="item">Owner name: <?php echo $ownerName?></div>
            <div class="item">Phone number: <?php echo $ownerPhone?></div>
            <div class="item">Location: <?php echo $ownerCity?></div>
            <div class="item">Zipcode: <?php echo $ownerZip?></div>
          </div>
        </div>
        <div class="description">
          <?php echo $objectDescription ?>
        </div>
      </div>

      <footer>
        <div class="column">
          <div class="title">Authors</div>
          <a href="#">Amariucai Ioana</a>
          <a href="#">Covrig Catalin</a>
          <a href="#">Osoeanu Florentina</a>
          <a href="#">Nicoleta Anca</a>
        </div>
        <div class="column">
          <div class="title">Teachers</div>
          <a href="#">Sabin Buraga</a>
          <a href="#">Coman Alexandru</a>
        </div>
        <div class="column">
          <div class="title">Project Related</div>
          <a href="https://bitbucket.org/auctioxteam/auctiox/overview" target="_blank">Project link</a>
          <a href="https://bitbucket.org/auctioxteam/auctiox/src" target="_blank">Source code</a>
        </div>
      </footer>
    </div>
  </body>
</html>
