## Synopsis

This is the client side of the Web Application named AuctioX. This app should offer to the user the the posibility to add personal object to an auction.

## Motivation

The main reason that we developed this project is to increase our skills in Web Technologies.

## API Reference

Temporary url: http://catalin-test-1.freetzi.com/login/login.html

## Scholarly HTML Report

Temporary url: http://catalin-test-1.freetzi.com/raport/auctiox.html

## Contributors

- Amariucai Ioana
- Covrig Catalin
- Osoeanu Florentina
- Nicoleta Anca
