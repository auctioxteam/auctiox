<?php
  $loginPageUrl = 'http://'.$_SERVER['HTTP_HOST'].'/app/login/signin.php';

  session_start();
  if (!isset($_SESSION['isLogedIn']) || $_SESSION['isLogedIn'] !== 'true'){
    header("Location: $loginPageUrl");
    exit();
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['signout'])){
      $_SESSION['isLogedIn'] = 'false';
      header("Location: $loginPageUrl");
      exit();
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AuctioX</title>
    <link rel="stylesheet" type="text/css" href="../core/_buttons.css">
    <link rel="stylesheet" type="text/css" href="../core/_inputs.css">
    <link rel="stylesheet" type="text/css" href="../core/_util.css">
    <link rel="stylesheet" type="text/css" href="../core/_search.css">
    <link rel="stylesheet" type="text/css" href="./_content.css">
    <script type="text/javascript" src="content.js"></script>
  </head>
  <body>

    <div class="main-page">
      <header>
        <div class="title" onclick="goTo('../content/content.php')">
          AuctioX
        </div>
        <div class="signout">
          Hey, <span class="user"><?php echo $_SESSION['firstname'].' '.$_SESSION['lastname']; ?></span> <form action="<?php $_SERVER['PHP_SELF']?>" method="post"><button type="submit" name="signout" class="button -small -standard" name="button">Sign Out</button></form>
        </div>
        <div class="search-bar">
          <button class="button -icon -small -standard" type="button" name="button" onclick="openAddObject()">
            <svg class="icon">
              <use xlink:href="../assets/img/svg/sprite.svg#plus"></use>
            </svg>
          </button>
          <button class="button -icon -small -standard" type="button" name="button" onclick="openStats()">
            <svg class="icon">
              <use xlink:href="../assets/img/svg/sprite.svg#stats-bars"></use>
            </svg>
          </button>
        </div>
      </header>

      <div class="middle">
        <aside>
          <div class="title">
            Filters
          </div>
          <form method="post">
            <div class="search-bar">
              <div class="search">
                <input type="text" name="mytext" placeholder="search">
                <svg class="icon">
                  <use xlink:href="../assets/img/svg/sprite.svg#search"></use>
                </svg>
              </div>
            </div>
            <div class="filter">
              <div class="title">
                Owner
              </div>
              <label><input type="radio" name="owner" value="all" checked>All</label>
              <label><input type="radio" name="owner" value="mine">Mine</label>
              <label><input type="radio" name="owner" value="others">Others</label>
            </div>
            <div class="filter">
              <div class="title">
                Sort by price
              </div>
              <label><input type="radio" name="sortByPrice" value="" checked>None</label>
              <label><input type="radio" name="sortByPrice" value="ascending">Ascending</label>
              <label><input type="radio" name="sortByPrice" value="descending">Descending</label>
              <div class="set-price">
                <input type="number" name="sortIntervalFrom" value="from" placeholder="from" class="input -small -standard"> -
                <input type="number" name="sortIntervalTo" value="to" placeholder="to" class="input -small -standard">
              </div>
            </div>
            <div class="filter">
              <div class="title">
                Sort by degree of wear
              </div>
              <label><input type="radio" name="degreeOfWear" value="" checked>None</label>
              <label><input type="radio" name="degreeOfWear" value="new">New</label>
              <label><input type="radio" name="degreeOfWear" value="used">Used</label>
            </div>
            <div class="filter">
              <div class="title">
                Sort by deadline
              </div>
              <label><input type="radio" name="sortByDeadline" value="false" checked name="test">No</label>
              <label><input type="radio" name="sortByDeadline" value="true">Yes</label>
            </div>
            <div class="filter">
              <div class="title">
                City
              </div>
              <select class="input -small" name="city">
                <option value="">-- Select a city --</option>
                <option value="Bucuresti">Bucuresti</option>
                <option value="Bluj">Cluj</option>
                <option value="Timisoara">Timisoara</option>
                <option value="Iasi">Iasi</option>
                <option value="Constanta">Constanta</option>
                <option value="Dolj">Dolj</option>
                <option value="Brasov">Brasov</option>
                <option value="Galati">Galati</option>
                <option value="Prahova">Prahova</option>
                <option value="Bihor">Bihor</option>
                <option value="Braila">Braila</option>
                <option value="Arad">Arad</option>
                <option value="Arges">Arges</option>
                <option value="Sibiu">Sibiu</option>
                <option value="Bacau">Bacau</option>
                <option value="Mures">Mures</option>
                <option value="Maramures">Maramures</option>
                <option value="Buzau">Buzau</option>
                <option value="Botosani">Botosani</option>
                <option value="Satu mare">Satu Mare</option>
                <option value="Valcea">Valcea</option>
                <option value="Mehedinti">Mehedinti</option>
                <option value="Suceava">Suceava</option>
                <option value="Neamt">Neamt</option>
                <option value="Gorj">Gorj</option>
                <option value="Dambovita">Dambovita</option>
                <option value="Vrancea">Vrancea</option>
                <option value="Nasaud">Nasaud</option>
                <option value="Severin">Severin</option>
                <option value="Tulcea">Tulcea</option>
                <option value="Olt">Olt</option>
                <option value="Calarasi">Calarasi</option>
                <option value="Alba">Alba</option>
                <option value="Giurgiu">Giurgiu</option>
                <option value="Deva">Deva</option>
              </select>
            </div>
            <div class="filter">
              <div class="title">
                Category
              </div>
              <select class="input -small" name="category">
                <option value="">-- Select a category --</option>
                <option value="collectibles">Collectibles &amp; art</option>
                <option value="electronics">Electronics</option>
                <option value="fashion">Fashion</option>
                <option value="home">Home &amp; garden</option>
                <option value="motors">Motors</option>
                <option value="musical">Muscial instrument &amp; gear</option>
                <option value="sporting">Sporting goods</option>
                <option value="toys">Toys &amp; hobbies</option>
                <option value="others">Others</option>
              </select>
              <input type="submit" name="Submit" value="Submit" class="button -auctiox -small">
            </div>
          </form>
        </aside>
        <section>
          <div class="objects-list">
            <?php
            include '../sqlInterrogations.php';
            $sqlInterrogation = new SQLInterrogation();



            if(isset($_POST['sortIntervalFrom'])) {
              $sortIntervalFrom = $_POST['sortIntervalFrom'];
            }
            else{
              $sortIntervalFrom = '';
            }

            if(isset($_POST['sortIntervalTo'])) {
              $sortIntervalTo = $_POST['sortIntervalTo'];
            }
            else{
              $sortIntervalTo='';
            }

            if(isset($_POST['sortByPrice'])) {
              //$result = $sqlInterrogation->getObjectsSortedByPrice($_POST['sortByPrice']);

            }
            if(isset($_POST['degreeOfWear'])) {
              $degreeOfWear = $_POST['degreeOfWear'];
            }

            if(isset($_POST['degreeOfWear'])) {
              $sortByDeadline = $_POST['sortByDeadline'];
            }

            $option = isset($_POST['category']) ? $_POST['category'] : false;
             if ($option) {
                $category = $_POST['category'];
             }else{
              $category = '';
             }

             $optionCity = isset($_POST['city']) ? $_POST['city'] : false;
             if ($optionCity) {
                $city = $_POST['city'];}
              else{
                $city='';
              }
              if(isset($_POST['mytext'])) {
                $mytext = $_POST['mytext'];
                /*$result = $sqlInterrogation->getObjectsSearchCriteria($mytext);
                echo $result[0]['name'];*/
              }
              else{
                $mytext ='';
              }

              if(isset($_POST['mytext'])) {
                $sortByPrice = $_POST['sortByPrice'];
              }



              if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if(isset($_POST['Submit'])){
                  $result = $sqlInterrogation->filter($_POST['owner'],$mytext, $city, $category, $sortByPrice, $degreeOfWear, $sortByDeadline, $sortIntervalFrom, $sortIntervalTo);
                  for ($i=0; $i <count($result)  ; $i++) {
                    echo '<div class="object" onClick="openObjectDetails(' . $result[$i]['idObject'] .   ')">';
                    echo '  <div class="left">';
                    echo '    <div class="img">';
                    echo '      <img src=' . 'http://' . $_SERVER['SERVER_NAME'] . '/app' . $result[$i]['image'] . ' alt=""/>';
                    echo '    </div>';
                    echo '    <div class="details">';
                    echo '      <div class="up">';
                    echo '        <div class="title">';
                    echo '          ' . $result[$i]['name'] . '';
                    echo '        </div>';
                    echo '      </div>';
                    echo '      <div class="down">';
                    echo '        <div class="category">';
                    echo '          ' . $result[$i]['category'] . '';
                    echo '        </div>';
                    echo '        >>';
                    echo '        <div class="subcategory">';
                    echo '          ' . $result[$i]['degreeOfWearing'] . '';
                    echo '        </div>';
                    echo '      </div>';
                    echo '    </div>';
                    echo '  </div>';
                    echo '  <div class="right">';
                    echo '    <div class="price">';
                    echo '          ' . $result[$i]['price'] . '$';
                    echo '    </div>';
                    echo '    <div class="dimensions">';
                    echo '       ' . date ( 'd-M-Y', (int)$result[$i]['enddate']) . '';
                    echo '    </div>';
                    echo '  </div>';
                    echo '</div>';
                  }
                }
              }else{
                $html = '';
                $url = '/app/object-feed.xml';
                $xml = simplexml_load_file('http://' . $_SERVER['SERVER_NAME'] . $url);
                $arrayLenght = count($xml->entry);

                for ($i=$arrayLenght-1; $i >=0 && $i >= $arrayLenght - 15 ; $i--) {
                  echo '<div class="object" onClick="openObjectDetails(' . $xml->entry[$i]->id .   ')">';
                  echo '  <div class="left">';
                  echo '    <div class="img">';
                  echo '      <img src=' . 'http://' . $_SERVER['SERVER_NAME'] . '/app' . $xml->entry[$i]->content->image . ' alt=""/>';
                  echo '    </div>';
                  echo '    <div class="details">';
                  echo '      <div class="up">';
                  echo '        <div class="title">';
                  echo '          ' . $xml->entry[$i]->title . '';
                  echo '        </div>';
                  echo '      </div>';
                  echo '      <div class="down">';
                  echo '        <div class="category">';
                  echo '          ' . $xml->entry[$i]->content->category . '';
                  echo '        </div>';
                  echo '        >>';
                  echo '        <div class="subcategory">';
                  echo '          ' . $xml->entry[$i]->content->subcategory . '';
                  echo '        </div>';
                  echo '      </div>';
                  echo '    </div>';
                  echo '  </div>';
                  echo '  <div class="right">';
                  echo '    <div class="price">';
                  echo '          ' . $xml->entry[$i]->content->price . '$';
                  echo '    </div>';
                  echo '    <div class="dimensions">';
                  echo '       ' . date ( 'd-M-Y', (int)$xml->entry[$i]->content->enddate ) . '';
                  echo '    </div>';
                  echo '  </div>';
                  echo '</div>';
                }
              }
              ?>
          </div>
        </section>
      </div>

      <footer>
        <div class="column">
          <div class="title">Authors</div>
          <a href="#">Amariucai Ioana</a>
          <a href="#">Covrig Catalin</a>
          <a href="#">Osoeanu Florentina</a>
          <a href="#">Nicoleta Anca</a>
        </div>
        <div class="column">
          <div class="title">Teachers</div>
          <a href="#">Sabin Buraga</a>
          <a href="#">Coman Alexandru</a>
        </div>
        <div class="column">
          <div class="title">Project Related</div>
          <a href="https://bitbucket.org/auctioxteam/auctiox/overview" target="_blank">Project link</a>
          <a href="https://bitbucket.org/auctioxteam/auctiox/src" target="_blank">Source code</a>
        </div>
      </footer>
    </div>

    <template id="object-template">
      <div class="object">
        <div class="left">
          <div class="img">
            <img src="http://www.apogeekits.com/images/6mm-slotted-screwdriver-large.jpg" alt=""/>
          </div>
          <div class="details">
            <div class="up">
              <div class="category">
                Category
              </div>
              >>
              <div class="title">
                Title
              </div>
            </div>
            <div class="down">
              <div class="location">
                Location
              </div>
              <div class="date">
                Date
              </div>
            </div>
          </div>
        </div>
        <div class="right">
          <div class="price">
            Price
          </div>
          <div class="dimensions">
            date
          </div>
        </div>
      </div>
    </template>

  </body>
</html>
