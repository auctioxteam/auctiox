// Date.prototype.ddmmyyyy = function() {
//   var yyyy = this.getFullYear().toString();
//   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
//   var dd  = this.getDate().toString();
//   return (dd[1]?dd:"0"+dd[0]) + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + yyyy; // padding
// };

// window.onload = function(){
//   this.objectList = (function(){
//     'use strict';
//
//     // form container
//     var OBJECT_ID = 'object-template';
//     var OBJECTS_ID = 'objects-list';
//     var _container = document.getElementById(OBJECTS_ID);
//
//     // get from server dates
//     function constructor(){
//       // refreshObjects(objects);
//     }
//
//     // replave form in DOM
//     function refreshObjects(objects){
//       // remove children
//       while (_container.firstChild) {
//           _container.removeChild(_container.firstChild);
//       }
//
//       // add children
//       addObjects(objects);
//     }
//
//     // add form as firstChild into container
//     function addObject(object){
//
//       // add to DOM
//       var element = document.getElementById(OBJECT_ID).content;
//       var clone = document.importNode(element, true);
//
//       _container.appendChild( updateClone(clone, object) );
//     }
//
//     // add form as firstChild into container
//     function addObjects(objects){
//       for(let i = 0; i < objects.length; i++ ){
//         addObject(objects[i]);
//       }
//     }
//
//     function updateClone(clone, object){
//       var title = clone.querySelector('.title');
//       title.textContent = object.title;
//
//       var category = clone.querySelector('.category');
//       category.textContent = object.category;
//
//       var location = clone.querySelector('.location');
//       location.textContent = object.location;
//
//       var date = clone.querySelector('.date');
//       date.textContent = object.date;
//
//       var date = clone.querySelector('.date');
//       date.textContent = 'available until ' + object.dateTo;
//
//       var price = clone.querySelector('.price');
//       price.textContent = object.price;
//
//       var price = clone.querySelector('.dimensions');
//       price.textContent = object.width + 'x' + object.height + ' (cm)';
//
//       var img = clone.querySelector('img');
//       img.src = object.image;
//
//       // attach click event
//       clone.firstElementChild.addEventListener("click", function(event) {
//         openObjectDetails(event);
//         console.log(event);
//       });
//
//       return clone;
//     }
//
//     constructor();
//
//     return {
//       refreshObjects
//     };
//   })();
// }


function openObjectDetails(id){
  // http things
  goTo('../object-details/object-details.php?' + id);
}

function openAddObject(){
  // http things
  goTo('../add-object/add-object.php');
}

function openStats(){
  // http things
  goTo('../stats/stats.php');
}

function goTo(path){
  window.location.href = path;
}
