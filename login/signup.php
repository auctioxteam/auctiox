<?php
include '../SQLInterrogations.php';
$errorMsg = '';
$mainPageUrl = 'http://'.$_SERVER['HTTP_HOST'].'/app/content/content.php';

session_start();
if (!isset($_SESSION['isLogedIn']) || $_SESSION['isLogedIn'] !== 'true'){
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['submit'])){
       $sqlInterogation = new SQLInterrogation();
       $result = $sqlInterogation->addUser(htmlspecialchars($_POST['signupEmail']),
                                            htmlspecialchars($_POST['signupFirst']),
                                            htmlspecialchars($_POST['signupLast']),
                                            // md5($_POST['signupPassword']),
                                            md5($_POST['signupPassword'].$_POST['signupEmail']),
                                            htmlspecialchars($_POST['signupCity']),
                                            htmlspecialchars($_POST['signupPhone']),
                                            htmlspecialchars($_POST['signupZip'])
                                        );
        if($result !== true){
          $errorMsg = $result;
        }else{
          // WIP go to login page
        }
    }
  }
}else{
  // redirect browser
  header("Location: $mainPageUrl");
  exit();
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AuctioX</title>
    <link rel="stylesheet" type="text/css" href="../core/_buttons.css">
    <link rel="stylesheet" type="text/css" href="../core/_inputs.css">
    <link rel="stylesheet" type="text/css" href="../core/_util.css">
    <link rel="stylesheet" type="text/css" href="./_login.css">
    <script type="text/javascript" src="login.js"></script>
  </head>
  <body>
    <div class="login-page">

      <div class="app-content">

        <div class="app-title">
          AuctioX
        </div>

        <div id="app-body">

          <form method="post" action="<?php $_SERVER['PHP_SELF']?>" class="form signup">
            <div class="box title">
              Signup
            </div>
            <div class="box">
              <input type="text" name="signupFirst" placeholder="First Name" class="input -standard -medium" pattern="[A-Za-z]*" title="A name should contain only letters" required>
              <input type="text" name="signupLast" placeholder="Last Name" class="input -standard -medium" pattern="[A-Za-z]*" title="A name should contain only letters" required>
              <input type="email" name="signupEmail" placeholder="email@yahoo.com" class="input -standard -medium" required>
              <input type="password" name="signupPassword" placeholder="New password" class="input -standard -medium" required>
              <!-- <input type="text" name="signupCity" placeholder="City" class="input -standard -medium" required> -->
              <select class="input -medium" name="signupCity" required>
                <option value="">-- Select a city --</option>
                <option value="Bucuresti">Bucuresti</option>
                <option value="Bluj">Cluj</option>
                <option value="Timisoara">Timisoara</option>
                <option value="Iasi">Iasi</option>
                <option value="Constanta">Constanta</option>
                <option value="Dolj">Dolj</option>
                <option value="Brasov">Brasov</option>
                <option value="Galati">Galati</option>
                <option value="Prahova">Prahova</option>
                <option value="Bihor">Bihor</option>
                <option value="Braila">Braila</option>
                <option value="Arad">Arad</option>
                <option value="Arges">Arges</option>
                <option value="Sibiu">Sibiu</option>
                <option value="Bacau">Bacau</option>
                <option value="Mures">Mures</option>
                <option value="Maramures">Maramures</option>
                <option value="Buzau">Buzau</option>
                <option value="Botosani">Botosani</option>
                <option value="Satu mare">Satu Mare</option>
                <option value="Valcea">Valcea</option>
                <option value="Mehedinti">Mehedinti</option>
                <option value="Suceava">Suceava</option>
                <option value="Neamt">Neamt</option>
                <option value="Gorj">Gorj</option>
                <option value="Dambovita">Dambovita</option>
                <option value="Vrancea">Vrancea</option>
                <option value="Nasaud">Nasaud</option>
                <option value="Severin">Severin</option>
                <option value="Tulcea">Tulcea</option>
                <option value="Olt">Olt</option>
                <option value="Calarasi">Calarasi</option>
                <option value="Alba">Alba</option>
                <option value="Giurgiu">Giurgiu</option>
                <option value="Deva">Deva</option>
              </select>
              <input type="number" min="0" name="signupPhone" placeholder="Phone number" class="input -standard -medium" required>
              <input type="number" min="0" name="signupZip" placeholder="Zip code" class="input -standard -medium" required>
              <div class="error">
                <?php echo $errorMsg ?>
              </div>
              <input type="submit" name="submit" value="Signup" class="button -auctiox -medium">
            </div>
            <div id="signupErrors">

            </div>
            <div class="box">
              <input type="button" name="login" value="Login" onClick="goToLogin()" class="button -standard -medium">
              <div class="">Contact us at suport@mailinator.com</div>
            </div>
          </form>

          <div class="testimonials">

            <div class="testimonial">
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#user-tie"></use>
              </svg>
              <q class="quotes">
                Compared to other websites AuctioX has an organized structure where you can access easily what are you searching for and it shows the best results for what I want to look for. It’s simple, fast and it shows the essential!
              </q>
            </div>

            <div class="testimonial">
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#user-tie"></use>
              </svg>
              <q class="quotes">
                I have been using AuctioX for over a year and I couldn’t be happier! This site shows the important announcements that are always updated which is a bonus for a merchandiser like me that is always looking for new products!
              </q>
            </div>

            <div class="testimonial">
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#user-tie"></use>
              </svg>
              <q class="quotes">
                The simple accessibility and the quick responses on the website are a bonus compared with other sites I was using before. It can be used by anyone with a little experience on using the computer and you can find any product you want to buy  only with some clicks.
              </q>
            </div>

          </div>
        </div>
      </div>
    </div>
  </body>
</html>
