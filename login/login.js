function goToSignup(){
  gotTo('./signup.php');
}

function goToReset(){
  gotTo('./resetPassword.php');
}

function goToLogin(){
  gotTo('./signin.php');
}

function gotTo(path){
  window.location.replace(path);
}
