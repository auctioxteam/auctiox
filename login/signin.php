<?php
include '../SQLInterrogations.php';
$errorMsg = '';
$mainPageUrl = 'http://'.$_SERVER['HTTP_HOST'].'/app/content/content.php';

session_start();
if (!isset($_SESSION['isLogedIn']) || $_SESSION['isLogedIn'] !== 'true'){
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['submit'])){

       $sqlInterogation = new SQLInterrogation();
       $result = $sqlInterogation->getUserByEmail($_POST['loginEmail']);
       $passwd = $result['password'];

      //  if( md5($_POST['loginPassword']) === $passwd){
       if( md5($_POST['loginPassword'].$_POST['loginEmail']) === $passwd){
         $_SESSION['isLogedIn'] = 'true';
         $_SESSION['idUser'] = $result['id'];
         $_SESSION['firstname'] = $result['firstname'];
         $_SESSION['lastname'] = $result['lastname'];
         $_SESSION['email'] = $result['email'];
         header("Location: $mainPageUrl");
         exit();
       }else{
         $errorMsg = 'Invalid email or password!';
       }
    }
  }
}else{
  // redirect browser
  header("Location: $mainPageUrl");
  exit();
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AuctioX</title>
    <link rel="stylesheet" type="text/css" href="../core/_buttons.css">
    <link rel="stylesheet" type="text/css" href="../core/_inputs.css">
    <link rel="stylesheet" type="text/css" href="../core/_util.css">
    <link rel="stylesheet" type="text/css" href="./_login.css">
    <script type="text/javascript" src="login.js"></script>
  </head>
  <body>
    <div class="login-page">

      <div class="app-content">

        <div class="app-title">
          AuctioX
        </div>

        <div id="app-body">

          <form action="<?php $_SERVER['PHP_SELF']?>" method="post" class="form login">
            <div class="box title">
              Login
            </div>
            <div class="box">
              <input type="email" name="loginEmail" placeholder="email@yahoo.com" class="input -standard -medium" required>
              <input type="password" name="loginPassword" placeholder="Password" class="input -standard -medium" required>
              <div class="error">
                <?php echo $errorMsg?>
              </div>
              <input type="submit" name="submit" value="Login" class="button -auctiox -medium">
            </div>
            <div class="box">
              <input type="button" name="signup" value="Signup" onClick="goToSignup()" class="button -standard -medium">
              <div class="">Contact us at suport@mailinator.com</div>
            </div>
          </form>

          <div class="testimonials">

            <div class="testimonial">
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#user-tie"></use>
              </svg>
              <q class="quotes">
                Compared to other websites AuctioX has an organized structure where you can access easily what are you searching for and it shows the best results for what I want to look for. It’s simple, fast and it shows the essential!
              </q>
            </div>

            <div class="testimonial">
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#user-tie"></use>
              </svg>
              <q class="quotes">
                I have been using AuctioX for over a year and I couldn’t be happier! This site shows the important announcements that are always updated which is a bonus for a merchandiser like me that is always looking for new products!
              </q>
            </div>

            <div class="testimonial">
              <svg class="icon">
                <use xlink:href="../assets/img/svg/sprite.svg#user-tie"></use>
              </svg>
              <q class="quotes">
                The simple accessibility and the quick responses on the website are a bonus compared with other sites I was using before. It can be used by anyone with a little experience on using the computer and you can find any product you want to buy  only with some clicks.
              </q>
            </div>

          </div>
        </div>
      </div>
    </div>
  </body>
</html>
