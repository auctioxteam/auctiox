<?php
class Connection{
  function Connection(){
    $this->SERVER_NAME="localhost";
    $this->USER_NAME="root";
    $this->DB="auctiox";
    // not protected yet
    $this->PASSWORD="password";
  }

  function makeConnectionDB(){
    try {
      $this->connect = new PDO("mysql:host=$this->SERVER_NAME;dbname=$this->DB", $this->USER_NAME);
      // set the PDO error mode to exception
      $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e){
      echo "Connection failed: " . $e->getMessage();
    }
    return $this->connect;
  }

  function closeConnectionDB(){
    $this->connect = null;
  }
}

?>
