<?php
class AddEntryToFeed{

  function AddEntryToFeed(){
    $url = '/app/object-feed.xml';
    $this->xml = simplexml_load_file('http://' . $_SERVER['SERVER_NAME'] . $url);
    $this->ARRAY_LENGHT = count($this->xml->entry);
  }

  function addEntry($id, $title, $published, $updated, $author, $category, $subcategory, $price, $endDate, $imagePath){

    // update feed
    $this->xml->updated = $published;

    // add to
    $this->xml->addChild('entry');
    $this->xml->entry[$this->ARRAY_LENGHT]->addChild('id', $id);   // id
    $this->xml->entry[$this->ARRAY_LENGHT]->addChild('title', $title);  //Second Object
    $this->xml->entry[$this->ARRAY_LENGHT]->addChild('published', $published); //1463513425436
    $this->xml->entry[$this->ARRAY_LENGHT]->addChild('updated', $updated); //1463513425436
    $this->xml->entry[$this->ARRAY_LENGHT]->addChild('author', $author); //Maria
    $this->xml->entry[$this->ARRAY_LENGHT]->addChild('content')->addAttribute("type", "xhtml");
    $this->xml->entry[$this->ARRAY_LENGHT]->content->addChild('category', $category); // Masini
    $this->xml->entry[$this->ARRAY_LENGHT]->content->addChild('subcategory', $subcategory); // Dacia
    $this->xml->entry[$this->ARRAY_LENGHT]->content->addChild('price', $price); // 12
    $this->xml->entry[$this->ARRAY_LENGHT]->content->addChild('enddate', $endDate); // 1463523425436
    $this->xml->entry[$this->ARRAY_LENGHT]->content->addChild('image', $imagePath); // '/assets/img/image/4f10e59a095e24b9cf7ac8e626b34d78e5b873fa.png

    // save changes
    $this->xml->asXML('../object-feed.xml');
  }

  // update entry -> price modified
  function updateEntryPrice($id, $newPrice, $time){
    $this->xml->updated = $time;
    for ($i=0; $i<$this->ARRAY_LENGHT; $i++) {
      if((int)$this->xml->entry[$i]->id == $id ){
        $this->xml->entry[$i]->content->enddate = $time;
        $this->xml->entry[$i]->content->price = $newPrice;
      }
    }
    // save changes
    $this->xml->asXML('../object-feed.xml');
  }
}
?>
