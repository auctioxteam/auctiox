<?php
  $loginPageUrl = 'http://'.$_SERVER['HTTP_HOST'].'/app/login/signin.php';

  session_start();
  if (!isset($_SESSION['isLogedIn']) || $_SESSION['isLogedIn'] !== 'true'){
    header("Location: $loginPageUrl");
    exit();
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['signout'])){
      $_SESSION['isLogedIn'] = 'false';
      header("Location: $loginPageUrl");
      exit();
    }
  }
?>
<?php
include '../SQLInterrogations.php';
$errorMsg = '';

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['submit'])){
      if ($_FILES["objectImage"]["size"] > 500000) {
         $errorMsg = "Sorry, your file is too large.";
      }else{
         $sqlInterrogation = new SQLInterrogation();
         $result = $sqlInterrogation->addObject(htmlspecialchars($_POST['objectCategory']),
                                               htmlspecialchars($_POST['objectSubcategory']),
                                               htmlspecialchars($_POST['degreeOfWear']),
                                               htmlspecialchars($_POST['objectName']),
                                               htmlspecialchars($_POST['objectPrice']),
                                               htmlspecialchars($_POST['objectDescription']),
                                               $_FILES['objectImage']
                                             );
          if($result !== true){
            $errorMsg = $result;
          }else{
            // WIP go to login page
          }
      }
    }
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AuctioX</title>
    <link rel="stylesheet" type="text/css" href="../core/_buttons.css">
    <link rel="stylesheet" type="text/css" href="../core/_inputs.css">
    <link rel="stylesheet" type="text/css" href="../core/_util.css">
    <!-- <link rel="stylesheet" type="text/css" href="./_add-object.css"> -->
    <link rel="stylesheet" type="text/css" href="../content/_content.css">
    <link rel="stylesheet" type="text/css" href="../login/_login.css">
    <script type="text/javascript" src="add-object.js"></script>
  </head>
  <body>
    <div class="main-page">

      <header>
        <div class="title" onclick="goTo('../content/content.php')">
          AuctioX
        </div>
        <div class="signout">
          Hey, <span class="user"><?php echo $_SESSION['firstname'].' '.$_SESSION['lastname']; ?></span> <form action="<?php $_SERVER['PHP_SELF']?>" method="post"><button type="submit" name="signout" class="button -small -standard" name="button">Sign Out</button></form>
        </div>
      </header>

      <form action="<?php $_SERVER['PHP_SELF']?>" enctype="multipart/form-data" method="post" class="form add-object">
        <div class="box title">
          Add Object
        </div>
        <div class="box">
          <select class="input -small" id="category" name="objectCategory" onchange="showSubcategory(this.selectedIndex)" required>
            <option value="">-- Select a category --</option>
            <option value="fashion">Fashion</option>
            <option value="home">Home &amp; garden</option>
            <option value="toys">Toys &amp; hobbies</option>
          </select>
          <select class="input -small" id="subcategory" name="objectSubcategory" required>
            <option value="">-- Select a subcategory --</option>
          </select>
          <input type="text" name="objectName" placeholder="Name" class="input -standard -medium" required>
          <input type="number" min="0" name="objectPrice" placeholder="Price ($)" class="input -standard -medium" required>
          <input type="file" accept='image/*' name="objectImage" placeholder="Image" class="input -standard -medium">
          <input type="textarea" name="objectDescription" placeholder="Description" class="input -standard -medium" rows="6" required>
          <div>
            <label><input type="radio" name="degreeOfWear" value="new" checked required>New</label>
            <label><input type="radio" name="degreeOfWear" value="old">Old</label>
          </div>
          <div class="error">
            <?php echo $errorMsg?>
          </div>
          <input type="submit" name="submit" value="Add Object" class="button -auctiox -medium">
        </div>
        <div class="box">
          <div class="">If you have problems, contact us at suport@mailinator.com</div>
        </div>
      </form>


      <footer>
        <div class="column">
          <div class="title">Authors</div>
          <a href="#">Amariucai Ioana</a>
          <a href="#">Balan Nicoleta Anca</a>
          <a href="#">Covrig Catalin</a>
          <a href="#">Osoeanu Florentina</a>
        </div>

        <div class="column">
          <div class="title">Project Related</div>
          <a href="https://bitbucket.org/auctioxteam/auctiox/overview" target="_blank">Project link</a>
          <a href="https://bitbucket.org/auctioxteam/auctiox/src" target="_blank">Source code</a>
        </div>

        <div class="column">
          <div class="title">Teachers</div>
          <a href="#">Sabin Buraga</a>
          <a href="#">Coman Alexandru</a>
        </div>
      </footer>

    </div>


  </body>
</html>
