var subcategories = {

  1: [{
      value: 'Clothes',
      label: 'Clothes'
    },{
      value: 'Shoes',
      label: 'Shoes'
    },{
      value: 'Accessory',
      label: 'Accessory'
    }
  ],
  2: [{
      value: 'Decor',
      label: 'Decor'
    },{
      value: 'tools',
      label: 'Tools'
    }
  ],
  3: [{
      value: 'Photography',
      label: 'Photography'
    },{
      value: 'Sports',
      label: 'Sports'
    },{
      value: 'Toys',
      label: 'Toys'
    }
  ]
}

function showSubcategory(value) {

  // clear options
  function clearOptions(select){
    var length = select.options.length;
    for (i = length-1; i >= 0; i--) {
      select.options[i] = null;
    }
  }

  var x = document.getElementById("subcategory");
  clearOptions(x);                  // clear subcategories
  var option = document.createElement("option");
  option.text = "-- Select a subcategory --";
  option.value = "";
  x.add(option);
  if (value > 0){

    for (id in subcategories[value]) {
      let option = document.createElement("option");
      option.text = subcategories[value][id].label;
      option.value = subcategories[value][id].value;
      x.add(option);
    }
  }
}

function openObjectDetails(id){
  // http things
  goTo('../object-details/object-details.php?' + id);
}

function openAddObject(){
  // http things
  goTo('../add-object/add-object.php');
}

function openStats(){
  // http things
  goTo('../stats/stats.php');
}

function goTo(path){
  window.location.href = path;
}
